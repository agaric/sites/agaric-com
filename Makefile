# Identify tasks which do not have output files:
.PHONY: help synclivetotest syncdb syncfiles

GREEN  := $(shell tput -Txterm setaf 2)
WHITE  := $(shell tput -Txterm setaf 7)
YELLOW := $(shell tput -Txterm setaf 3)
RESET  := $(shell tput -Txterm sgr0)

# Perl to convert target docs (following ##@).
# Example:
# mytarget:	##@group description
# Group items under @group in output,
# And show target<tab>description
HELPME = \
	%help; \
	while(<>) { push @{$$help{$$2 // 'options'}}, [$$1, $$3] if /^([a-zA-Z\-]+)\s*:.*\#\#(?:@([a-zA-Z\-]+))?\s(.*)$$/ }; \
	for (sort keys %help) { \
	print "${WHITE}$$_:${RESET}\n"; \
	for (@{$$help{$$_}}) { \
	$$sep = " " x (20 - length $$_->[0]); \
	print "  ${YELLOW}$$_->[0]${RESET}$$sep${GREEN}$$_->[1]${RESET}\n"; \
	}; \
	print "\n"; }

# Should always be the first target (as this will run with just `make`):
help:	##@Targets Show this text (or just run make)
	@perl -e '$(HELPME)' Makefile

# Configure sync'
ALIAS_LIVE := @live.live
ALIAS_TEST := @test.test
SRV_TEST := root@elizabeth.mayfirst.org
SRV_LIVE := $(SRV_TEST)
FILES_LIVE := /home/agaric_live/files
FILES_TEST := /home/agaric_test/files
ECHO := $(if $(TEST),@echo)
synclivetotest:	##@Targets Sync live environment to test (pass TEST=true to echo commands)
	$(ECHO) drush -y $(ALIAS_TEST) sql-drop
	$(ECHO) drush -y sql-sync $(ALIAS_LIVE) $(ALIAS_TEST)
ifeq ($(SRV_TEST), $(SRV_LIVE))
	$(ECHO) ssh -A $(SRV_LIVE) rsync -rz --stats --exclude styles \
	 --exclude css --exclude js $(FILES_LIVE) \
	 --delete ${FILES_TEST}
else
	$(ECHO) drush -y sync $(ALIAS_LIVE)/public/%files $(ALIAS_TEST)/public/%files
endif

		
