<?php

namespace Drupal\agaric_migration\Plugin\migrate\process;

use Drupal\migrate_plus\Plugin\migrate\process\EntityGenerate;

/**
 * RoleGenerate migrate plugin.
 *
 * Basically just extends the EntityGenerate module and overwrite the entity
 * method and there just set a label to the role.
 *
 * @MigrateProcessPlugin(
 *   id = "role_generate"
 * )
 *
 * @see EntityGenerate
 */
class RoleGenerate extends EntityGenerate {

  /**
   * Gives a proper label to the role.
   *
   * @param mixed $value
   *   Primary value to use in creation of the entity.
   *
   * @return array
   *   Entity value array.
   */
  protected function entity($value) {
    $entity_values = parent::entity($value);
    $entity_values['label'] = ucfirst($value);
    return $entity_values;
  }

}
