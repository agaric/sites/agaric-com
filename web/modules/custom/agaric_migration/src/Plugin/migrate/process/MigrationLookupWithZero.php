<?php

namespace Drupal\agaric_migration\Plugin\migrate\process;

use Drupal\migrate\MigrateSkipProcessException;
use Drupal\migrate\Plugin\migrate\process\MigrationLookup;

/**
 * Looks up the value of a property based on a previous migration.
 *
 * This plugin just alters the MigrationLookup::skipOnEmpty to consider the
 * number zero as a valid ID.
 *
 * @MigrateProcessPlugin(
 *   id = "migration_lookup_with_zero"
 * )
 */
class MigrationLookupWithZero extends MigrationLookup {

  /**
   * {@inheritdoc}
   */
  protected function skipOnEmpty(array $value) {
    if (!array_filter($value, [$this, 'isNotEmpty'])) {
      throw new MigrateSkipProcessException();
    }
  }

  /**
   * Determines if the value is not empty.
   *
   * The only values considered empty are: NULL, FALSE and an empty string.
   *
   * @param string $value
   *   The value to test if is empty.
   *
   * @return bool
   *   Return true if the value is not empty.
   */
  protected function isNotEmpty($value) {
    return ($value !== NULL && $value !== FALSE && $value !== '');
  }

}
