#!/bin/bash
set -euo pipefail
# Confirmed with echo `pwd` that as long as we use via composer it's always in
# /var/www/html (aka the project root).
proj_dir="$(cd $(dirname ${0})/../ && pwd)" # Parent of this script folder.
[ "$proj_dir" != "/var/www/html" ] && { echo "Script running from unexpected path - are you running within ddev, as you should?"; exit 1; }
local_backup="$(date +%Y%m%dT%H%M%S)_pre_pull.sql"
[ ! -d /var/www/html/backups ] && mkdir /var/www/html/backups || true

verbose=""
backup="false"
sync="false"
target="local"

while [ $# -gt 0 ]
do
  case $1
  in
    -t|--target)
      target=$2
      shift 2
    ;;

    -b|--backup)
      backup="true"
      shift 1
    ;;

    -v|--verbose)
      verbose="-v"
      shift 1
    ;;

    -s|--sync)
      sync="true"
      shift 1
    ;;

    *)
      echo "The arguments to use are"
      echo "-t|--target {local|test}: The target to load the database to"
      echo "-b|--backup: Indicate that a new backup should be made of the live database"
      echo "-v|--verbose: Wordier output"
      echo "-s|--sync: syncs files from live site"
      exit 1
    ;;
  esac
done

if [ "$backup" = "true" ]; then
  echo "Backing up live database..."
  live_backup="prod_$(date +%Y%m%dT%H%M%S).sql"
  remote_drush="/home/agaric_live/site/vendor/bin/drush -r /home/agaric_live/site/web $verbose"
  ssh_cmd="$remote_drush sql-dump --gzip --structure-tables-key=common --result-file=~/backups/$live_backup"
  ssh agaric_live@elizabeth.mayfirst.org $ssh_cmd
  live_backup="${live_backup}.gz"
else
  echo "Identifying the most recent live database backup..."
  live_backup=`ssh agaric_live@elizabeth.org "ls -1t ~/backups/prod*.sql.gz | head -n1 | xargs -n1 basename"`
fi

if [ "$target" = "local" ]; then
  echo "Copying the database file from live to local..."
  scp agaric_live@elizabeth.mayfirst.org:~/backups/${live_backup} /var/www/html/backups

  echo "Backing up local database to backups/${local_backup}..."
  drush sql-dump > /var/www/html/backups/${local_backup}

  echo "Dropping local database..."
  drush -y sql-drop

  echo "Unzipping live database..."
  gzip -d -f /var/www/html/backups/${live_backup}

  echo "Importing the database copied from live..."
  drush sqlc < /var/www/html/backups/${live_backup%.*}

  if [ "$sync" = "true" ]; then
    echo "Pulling files from live into local..."
    drush -y rsync @live:%files @self:%files
  fi

elif [ "$target" = "test" ]; then
  remote_drush="/home/agaric_test/site/vendor/bin/drush -r /home/agaric_test/site/web $verbose"
  ssh agaric_test@elizabeth.mayfirst.org "${remote_drush} status > /dev/null" || (echo "SSH connection to ${target} or drush status failed - have you set up the key, and does site exist?"; exit 1)
  ssh_cmd="scp ~/backups/${live_backup} agaric_test@localhost:~/backups"
  ssh agaric_live@elizabeth.mayfirst.org ${ssh_cmd}

  echo "Backing up test database..."
  ssh agaric_test@elizabeth.mayfirst.org "${remote_drush} sql-dump > ~/backups/${local_backup}"

  echo "Dropping test database..."
  drush @test -y sql-drop

  echo "Unzipping live database..."
  ssh_cmd="gzip -d -f ~/backups/${live_backup}"
  ssh agaric_test@elizabeth.mayfirst.org "${ssh_cmd}"

  echo "Loading live database into test..."
  ssh agaric_test@elizabeth.mayfirst.org "${remote_drush} sqlc < ~/backups/${live_backup%.*}"

  if [ "$sync" = "true" ]; then
    echo "Pulling files from live into test..."
    drush -y rsync @live:%files @test:%files
  fi
fi

echo "Done"
