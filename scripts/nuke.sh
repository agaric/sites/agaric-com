# Delete Drupal core and any contrib directories.
rm -rf web/core
rm -rf web/modules/contrib
rm -rf web/themes/contrib
rm -rf web/profiles/contrib
rm -rf drush/Commands/contrib
 
# Delete Drupal root files.
rm -f web/.csslintrc
rm -f web/.editorconfig
rm -f web/.eslintignore
rm -f web/.eslintrc.json
rm -f web/.gitattributes
rm -f web/.gitignore
rm -f web/.ht.router.php
rm -f web/autoload.php
rm -f web/example.gitignore
rm -f web/index.php
rm -f web/INSTALL.txt
rm -f web/robots.txt
rm -f web/README.txt
rm -f web/update.php
rm -f web/web.config
 
# Delete any Drupal scaffold files.
rm -f .editorconfig
rm -f .gitattributes
 
# Delete any third party libraries.
# rm -rf web/libraries
 
# Delete the vendor directory.
rm -rf vendor
